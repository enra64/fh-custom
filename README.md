# Flathunter

A bot to help people with their rental real-estate search. 🏠🤖

## Description

This is a branch of [Flathunter](https://github.com/flathunters/flathunter) that I updated. My changes include

* replace immoscout scraper with a crawler that uses their mobile API
* sending images on telegram (by now also supported in master)
* add coreberlin realtor
* add inberlinwohnen.de, an aggregator for public housing developers in berlin. cheap flats, but good luck getting one on a good salary...
* add support for filtering by distance to one of the gmaps targets
* add some more filters, like no WBS, balcony required
* The only available messaging service is [Telegram](https://telegram.org/), which was available at time of fork
* add graphs to show you how your search is behaving:

![img.png](readme_images/size_price_graph.png)
![img.png](readme_images/rejection_reasons_graph.png)

## Table of Contents
- [Background](#background)
- [Install](#install)
  - [Prerequisites](#prerequisites)
  - [Installation on Linux](#installation-on-linux)
- [Usage](#usage)
  - [Docker](#docker)
  - [Configuration](#configuration)
    - [URLs](#urls)
    - [Telegram](#telegram)
    - [Google API](#google-api)
  - [Command-line Interface](#command-line-interface)
- [Maintainers](#maintainers)
- [Credits](#credits)
  - [Contributers](#contributers)
- [Contributing](#contributing)

## Background for Berlin
IMO the only usable services when you want a flat for yourself in Berlin are immowelt and immoscout.
You can add kleinanzeigen and wg-gesucht if you want, 
might also be that they're a lot more useful if you're looking for a shared flat.

In my experience, new flats on immoscout are gone at the maximum within 30 minutes.
Try to reply in the first 2 minutes.

## Usage

### Docker

First EITHER build the image inside the project's root directory:
```sh
$ docker build -t flathunter .
```

OR use the one from gitlab, e.g. in docker-compose (config (ro) and db dir (rw) must be accessible to user 999):

```
services:
  flatbot:
    image: registry.gitlab.com/enra64/fh-custom:main
    restart: always
    volumes:
     - /etc/localtime:/etc/localtime:ro
     - ./config.yaml:/config/config.yaml
     - flathunter-dbs:/databases
```

### Configuration

Before running the project for the first time, copy `config.yaml.dist` to `config.yaml`. The `urls` and `telegram` sections of the config file must be configured according to your requirements before the project will run. 

#### URLs

To configure the searches, simply visit the property portal of your choice (e.g. ImmoScout24), configure the search on the website to match your search criteria, then copy the URL of the results page into the config file. You can add as many URLs as you like, also multiple from the same website if you have multiple different criteria (e.g. running the same search in different areas).

 * Currently, eBay Kleinanzeigen, Immowelt, WG-Gesucht and Idealista only crawl the first page, so make sure to **sort by newest offers**.
 * Your links should point to the German version of the websites (in the case of eBay Kleinanzeigen, Immowelt, ImmoScout24 and WG-Gesucht), since it is tested only there. Otherwise you might have problems.
 * follow the info in the example config yaml to craft a URL for immoscout mobile 


#### Telegram

To be able to send messages to you over Telegram, you need to register a new bot with the [BotFather](https://telegram.me/BotFather) for `Flathunter` to use. Through this process, a "Bot Token" will be created for you, which should be configured under `bot_token` in the config file.

To know who should Telegram messages should be sent to, the "Chat IDs" of the recipients must be added to the config file under `receiver_ids`. To work out your own Chat ID, send a message to your new bot, then run:

```
$ curl https://api.telegram.org/bot[BOT-TOKEN]/getUpdates
```

to get list of messages the Bot has received. You will see your Chat ID in there.

#### Google API

To use the distance calculation feature a [Google API-Key](https://developers.google.com/maps/documentation/javascript/get-api-key) is needed, as well as to enable the [Distance Matrix API](https://developers.google.com/maps/documentation/distance-matrix/overview) (This is NOT free).

### Command-line Interface

By default, the application runs on the commandline and outputs logs to `stdout`. It will poll in a loop and send updates after each run. The `processed_ids.db` file contains details of which listings have already been sent to the Telegram bot - if you delete that, it will be recreated, and you may receive duplicate listings.

```
usage: flathunt.py [-h] [--config CONFIG]

Searches for flats on Immobilienscout24.de and wg-gesucht.de and sends results
to Telegram User

optional arguments:
  -h, --help            show this help message and exit
  -m                    set how to store the known IDs. 
                        If you set it to `memory` (ie -m memory), it'll be kept in RAM. 
                        Useful for developing.
                        Any other value or leaving it out uses a local SQLite 
  --config CONFIG, -c CONFIG
                        Config file to use. If not set, try to use
                        '~git-clone-dir/config.yaml'
  --heartbeat INTERVAL, -hb INTERVAL
			Set the interval time to receive heartbeat messages to check that the bot is
                        alive. Accepted strings are "hour", "day", "week". Defaults to None.
```

## Maintainers

This project is maintained by the members of the [Flat Hunters](https://github.com/flathunters) Github organisation, which is a collection of individual unpaid volunteers who have all had their own processes with flat-hunting in Germany. If you want to join, just ping one of us a message!

Also by me, enra64.

## Credits

The original code was contributed by [@NodyHub](https://github.com/NodyHub), whose original idea this project was.

### Contributers

Other contributions were made along the way by:

- Bene
- [@tschuehly](https://github.com/tschuehly)
- [@Cugu](https://github.com/Cugu)
- [@GerRudi](https://github.com/GerRudi)
- [@xMordax](https://github.com/xMordax)
- [@codders](https://github.com/codders)
- [@alexanderroidl](https://github.com/alexanderroidl)

## Contributing

send me a PR and I will review it
