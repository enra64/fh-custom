"""Expose crawler for inberlinwohnen"""
import logging
import re

from flathunter.abstract_crawler import Crawler
from flathunter.inberlinwohnen.scraper.flats import get_flat_data_from_soup
from flathunter.inberlinwohnen.scraper.model.flat import flat_to_fh_dict


class CrawlInBerlinWohnen(Crawler):
    """Implementation of Crawler interface for Ebay Kleinanzeigen"""

    __log__ = logging.getLogger('flathunt')
    USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'
    URL_PATTERN = re.compile(r'(gewobag|degewo|wbm|gesobau|howoge|stadtundland|inberlinwohnen)')

    def __init__(self, config):
        super().__init__(config)
        logging.getLogger("requests").setLevel(logging.WARNING)
        self.config = config

    def get_page(self, search_url, driver=None, page_no=None):
        """Applies a page number to a formatted search URL and fetches the exposes at that page"""
        return self.get_soup_from_url(search_url)

    # pylint: disable=too-many-locals
    def extract_data(self, soup):
        """Extracts all exposes from a provided Soup object"""
        flats = get_flat_data_from_soup(soup)
        entries = list(map(lambda f: flat_to_fh_dict(f), flats))

        self.__log__.debug('extracted: %d', len(entries))

        return entries

    def get_expose_details(self, expose):
        """Loads additional detalis for an expose. Should be implemented in the subclass"""
        source = expose['url']
        soup = self.get_page(source)
        image_elements = []
        expose['photos'] = []
        base_url = ""

        if 'gesobau' in source:
            image_elements = [link for link in soup.select('div.slide-item img') if "typo3temp" in link.attrs['src']]
            base_url = "www.gesobau.de"
        elif 'gewobag' in source:
            image_elements = soup.select(".rental-overview .overview-media img")
            base_url = "www.gewobag.de"
        elif 'degewo' in source:
            # image_elements = [link for link in soup.select('.lazyloaded') if "thumbs" not in link.attrs['srcset']]
            # base_url = "immosuche.degewo.de"
            pass
        elif 'gesobau' in source:
            image_elements = soup.select('div.element-media img')
            base_url = "www.gesobau.de"
        elif 'howoge' in source:
            pass
            # image_elements = soup.select('figure.swiper-slide img')
            # base_url = "www.howoge.de"
        elif 'stadtundland' in source:
            image_elements = soup.select('.SP-ImageSlider figure img')
            base_url = "www.stadtundland.de"

        base_url = "https://" + base_url

        for img in image_elements:
            photo = None
            if 'data-src' in img.attrs:
                photo = img.attrs["data-src"]
            elif 'src' in img.attrs:
                photo = img.attrs["src"]
            elif 'srcset' in img.attrs:
                try:
                    photo = list(sorted(img.attrs["srcset"].split(","), key=lambda x: -int(x.split()[1].replace("w", ""))))[0].split()[0]
                except:
                    candidates = img.attrs["srcset"].split(" ")
                    for c in candidates:
                        if "jpg" in c or "webp" in c or "png" in c or "gif" in c:
                            photo = c
                            break

            if photo is not None:
                is_absolute_url = "https" in photo
                if is_absolute_url:
                    expose['photos'].append(photo)
                else:
                    expose['photos'].append(base_url + photo)

        return expose
