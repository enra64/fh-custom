import logging
import time
import traceback
from typing import List

import requests
from bs4 import BeautifulSoup
from requests import ReadTimeout, ConnectTimeout

from flathunter.inberlinwohnen.scraper.model.flat import Flat

__log__ = logging.getLogger('flathunt')

def get_room_count(child):
    try:
        return child.select("span > span > strong")[0].text.strip()
    except:
        traceback.print_exc()
        return "unable to get room count"


def get_title(child):
    try:
        return child.select_one("h2").contents[0]
    except:
        traceback.print_exc()
        return "unable to get title"




def get_balcony(child):
    try:
        balcony_tags = child.select(".balkon_loggia_terrasse")
        return len(balcony_tags) > 0
    except:
        traceback.print_exc()
        return "unable to get room count"


def get_wbs_required(child):
    try:
        wbs_tags = child.select(".wbs")
        return len(wbs_tags) > 0
    except:
        traceback.print_exc()
        return "unable to get room count"

def get_area_sqm(child):
    try:
        return child.select("span > span > strong")[1].text.strip()
    except:
        traceback.print_exc()
        return "unable to get area"


def get_price(child):
    try:
        return child.select("span > span > strong")[2].text.strip()
    except:
        traceback.print_exc()
        return "unable to get price"


def get_address(child):
    try:
        return child.select("span > span._tb_left")[0].text.split("|")[1].strip()
    except:
        traceback.print_exc()
        return "unable to get address "


def get_link(child):
    try:
        href = child.select("a.org-but")[0].attrs["href"]
        full_link = "http://inberlinwohnen.de/" + href.replace(" ", "%20").strip("/")
        try:
            result = requests.head(full_link, allow_redirects=True, timeout=3).url
            time.sleep(0.1)  # don't hammer inberlinwohnen.de with HEAD requests
            if "404" in result:
                return full_link
            else:
                return result
        except ConnectTimeout:
            __log__.info(f"Couldn't resolve (ConnectTimeout) target URL for flat {full_link}")
            return full_link
        except ReadTimeout:
            __log__.info(f"Couldn't resolve (ReadTimeout) target URL for flat {full_link}")
            return full_link
        except Exception as e:
            __log__.warning(f"Couldn't resolve target URL for flat {full_link}", exc_info=e)
            return full_link
    except Exception as e:
        __log__.warning("Couldn't find URL in flat soup", exc_info=e)
        return "unable to get link"


def get_realtor(child):
    try:
        resolved_link = get_link(child)
        if "howoge" in resolved_link:
            return "howoge"
        elif "degewo" in resolved_link:
            return "degewo"
        elif "wbm" in resolved_link:
            return "wbm"
        elif "gewobag" in resolved_link:
            return "gewobag"
        elif "stadtundland" in resolved_link:
            return "stadtundland"
        else:
            return "N/A"
    except:
        traceback.print_exc()
        return "unable to get link"

def get_image(child):
    try:
        href = child.select("figure")[0].attrs["style"].replace("background-image:url(", "").replace(");", "")
        return href.replace(" ", "%20").strip("/")
    except:
        traceback.print_exc()
        return "unable to get image"


def get_available_from(child):
    try:
        return child.select('.my_tp_dt')[0].text
    except:
        traceback.print_exc()
        return "unable to get available from date"


def get_id(child):
    try:
        return int(child.attrs["id"].replace("flat_", ""))
    except:
        traceback.print_exc()
        return "unable to get id"


def soupify(html_doc):
    try:
        parsed = BeautifulSoup(html_doc.content, "html.parser")
        try:
            children_tags = parsed.select("#_tb_relevant_results > li")
            return children_tags
        except:
            traceback.print_exc()
            print("couldn't select relevant results")
    except:
        traceback.print_exc()
        print("Couldn't create beautifulsoup")
    return None


def get_flat_data_from_soup(soup) -> List[Flat]:
    scrape = soup.select("#_tb_relevant_results > li")

    if scrape is None:
        print("ooph why no scrape?")
        return []

    # only get 15 newest ones
    scrape = scrape[:3]

    result = []

    for child in scrape:
        result.append(
            Flat(
                get_id(child),
                get_room_count(child),
                get_area_sqm(child),
                get_price(child),
                get_address(child),
                get_link(child),
                get_available_from(child),
                get_image(child),
                get_balcony(child),
                get_wbs_required(child),
                get_realtor(child),
                get_title(child)
            )
        )

    return result

