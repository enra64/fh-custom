from dataclasses import dataclass
from typing import Dict


@dataclass
class Flat:
    id: str
    room_count: str
    area_sqm: str
    price: str
    address: str
    link: str
    available_from: str
    image: str
    has_balcony: bool
    requires_wbs: bool
    realtor: str
    title: str


def flat_to_fh_dict(flat: Flat) -> Dict:
    return {
        'id': hash(flat.id),
        'image': flat.image,
        'url': flat.link,
        'price': flat.price,
        'size': flat.area_sqm,
        'rooms': flat.room_count,
        'address': flat.address,
        'crawler': "inberlinwohnen",
        'from': flat.available_from,
        'has_balcony': flat.has_balcony,
        'requires_wbs': flat.requires_wbs,
        'realtor': flat.realtor,
        'title': flat.title,
    }