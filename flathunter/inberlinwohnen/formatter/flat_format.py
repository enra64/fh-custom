from typing import List

from flathunter.inberlinwohnen.scraper.model.flat import Flat


def _clean(msg: str) -> str:
    bad_chars = [
        "_",
        "*",
        "[",
        "]",
        "(",
        ")",
        "~",
        "`",
        ">",
        "#",
        "+",
        "-",
        "=",
        "|",
        "{",
        "}",
        ".",
        "!",
    ]
    for char in bad_chars:
        msg = msg.replace(char, f"\\{char}")

    return msg


def _map_flat(flat: Flat) -> str:
    link = f"{flat.link}"
    description = (
        f"{flat.room_count} Zimmer, {flat.area_sqm} m², {flat.price}€, {flat.address}"
    )
    clean_description = _clean(description)
    return f"[{clean_description}]({link})"


def format_flats(flats: List[Flat]) -> str:
    mapped = [_map_flat(f) for f in flats]
    return "\n\n".join(mapped)
