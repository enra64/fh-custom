"""Module with implementations of standard expose filters"""
import logging
from collections import defaultdict
from functools import reduce
import re
from typing import Dict

from flathunter.idmaintainer import AlreadySeenFilter


class ExposeHelper:
    """Helper functions for extracting data from expose text"""

    @staticmethod
    def get_price(expose):
        """Extracts the price from a price text"""
        price_match = re.search(r'\d+([\.,]\d+)?', expose['price'])
        if price_match is None:
            return None
        return float(price_match[0].replace(".", "").replace(",", "."))

    @staticmethod
    def get_size(expose):
        """Extracts the size from a size text"""
        size_match = re.search(r'\d+([\.,]\d+)?', expose['size'])
        if size_match is None:
            return None
        return float(size_match[0].replace(",", "."))

    @staticmethod
    def get_rooms(expose):
        """Extracts the number of rooms from a room text"""
        rooms_match = re.search(r'\d+([\.,]\d+)?', expose['rooms'])
        if rooms_match is None:
            return None
        return float(rooms_match[0].replace(",", "."))


class WbsFilter:
    """Exclude exposes that require a WBS"""

    def __init__(self, user_has_wbs):
        self.user_has_wbs = user_has_wbs

    def rejection_reason(self, expose):
        return "requires WBS"

    def is_interesting(self, expose):
        if 'requires_wbs' in expose:
            if self.user_has_wbs:
                return True
            else:
                return not expose['requires_wbs']
        else:
            return True


class MaxPriceFilter:
    """Exclude exposes above a given price"""

    def __init__(self, max_price):
        self.max_price = max_price

    def rejection_reason(self, expose):
        expose_price = ExposeHelper.get_price(expose)
        return f"too expensive ({expose_price} gt {self.max_price})"

    def is_interesting(self, expose):
        """True if expose is below the max price"""
        price = ExposeHelper.get_price(expose)
        if price is None:
            return True
        return price <= self.max_price


class MaxTravelDurationFilter:
    """Exclude exposes that take more than a certain duration to get there"""

    def __init__(self, max_duration_config: Dict[str, Dict[str, int]]):
        """
        :param max_duration_config: A dictionary of dictionaries. Dict[name][mode] = max_duration_minutes
        """
        self.max_duration_config = max_duration_config

    def rejection_reason(self, expose):
        far_stuff = self._list_too_far_stuff(expose)
        far_stuff_printed = "; ".join(far_stuff)
        return f"too far away ({far_stuff_printed})"

    def _list_too_far_stuff(self, expose):
        too_far_names = []
        expose_durations: Dict[str, Dict[str, int]] = expose['machine_readable_durations']
        for travel_target_name, name_durations in expose_durations.items():
            for travel_mode, expose_travel_duration in name_durations.items():
                duration_has_known_maximum = travel_target_name in self.max_duration_config and \
                                             travel_mode in self.max_duration_config[travel_target_name]
                if duration_has_known_maximum:
                    max_duration = self.max_duration_config[travel_target_name][travel_mode]
                    if expose_travel_duration > max_duration:
                        too_far_names.append(f"{travel_target_name} {travel_mode} {expose_travel_duration:.0f}min")

        return too_far_names

    def is_interesting(self, expose):
        """True if expose meets all max duration criteria"""
        too_far = self._list_too_far_stuff(expose)
        return len(too_far) == 0


class HasBalconyFilter:
    """Exclude exposes without a balcony"""

    def __init__(self, require_balcony):
        self.require_balcony = bool(require_balcony)

    def rejection_reason(self, expose):
        return "no balcony"

    def is_interesting(self, expose):
        """True if expose is below the max price"""
        if self.require_balcony:
            if 'has_balcony' in expose and expose['has_balcony'] is not None:
                return expose['has_balcony']
            else:
                return True
        return True


class MinPriceFilter:
    """Exclude exposes below a given price"""

    def __init__(self, min_price):
        self.min_price = min_price

    def rejection_reason(self, expose):
        expose_price = ExposeHelper.get_price(expose)
        return f"too cheap ({expose_price} lt {self.min_price})"

    def is_interesting(self, expose):
        """True if expose is above the min price"""
        price = ExposeHelper.get_price(expose)
        if price is None:
            return True
        return price >= self.min_price


class MaxSizeFilter:
    """Exclude exposes above a given size"""

    def __init__(self, max_size):
        self.max_size = max_size

    def rejection_reason(self, expose):
        expose_size = ExposeHelper.get_size(expose)
        return f"too big ({expose_size} gt {self.max_size})"

    def is_interesting(self, expose):
        """True if expose is below the max size"""
        size = ExposeHelper.get_size(expose)
        if size is None:
            return True
        return size <= self.max_size


class MinSizeFilter:
    """Exclude exposes below a given size"""

    def __init__(self, min_size):
        self.min_size = min_size

    def rejection_reason(self, expose):
        expose_size = ExposeHelper.get_size(expose)
        return f"too small ({expose_size} lt {self.min_size})"

    def is_interesting(self, expose):
        """True if expose is above the min size"""
        size = ExposeHelper.get_size(expose)
        if size is None:
            return True
        return size >= self.min_size


class MaxRoomsFilter:
    """Exclude exposes above a given number of rooms"""

    def __init__(self, max_rooms):
        self.max_rooms = max_rooms

    def rejection_reason(self, expose):
        rooms = ExposeHelper.get_rooms(expose)
        return f"too many rooms ({rooms} gt {self.max_rooms})"

    def is_interesting(self, expose):
        """True if expose is below the max number of rooms"""
        rooms = ExposeHelper.get_rooms(expose)
        if rooms is None:
            return True
        return rooms <= self.max_rooms


class MinRoomsFilter:
    """Exclude exposes below a given number of rooms"""

    def __init__(self, min_rooms):
        self.min_rooms = min_rooms

    def rejection_reason(self, expose):
        rooms = ExposeHelper.get_rooms(expose)
        return f"too few rooms ({rooms} lt {self.min_rooms})"

    def is_interesting(self, expose):
        """True if expose is above the min number of rooms"""
        rooms = ExposeHelper.get_rooms(expose)
        if rooms is None:
            return True
        return rooms >= self.min_rooms


class TitleFilter:
    """Exclude exposes whose titles match the provided terms"""

    def __init__(self, filtered_titles):
        self.filtered_titles = filtered_titles

    def _forbidden_words(self, expose):
        combined_excludes = "(" + ")|(".join(self.filtered_titles) + ")"
        found_objects = re.search(combined_excludes, expose['title'], re.IGNORECASE)
        return found_objects

    def rejection_reason(self, expose):
        forbidden_words = self._forbidden_words(expose)
        forbidden_words_formatted = str(", ".join(list(filter(lambda s: s is not None, list(forbidden_words.groups())))))
        return f"forbidden title ({forbidden_words_formatted})"

    def is_interesting(self, expose):
        """True unless title matches the filtered titles"""
        found_objects = self._forbidden_words(expose)

        if not found_objects:
            return True
        return False


class PPSFilter:
    """Exclude exposes above a given price per square"""

    def __init__(self, max_pps):
        self.max_pps = max_pps

    def rejection_reason(self, expose):
        pps = self._pps(expose)
        return f"too expensive (PPS {pps:.2f})"

    def _pps(self, expose):
        size = ExposeHelper.get_size(expose)
        price = ExposeHelper.get_price(expose)
        if size is None or price is None:
            return True
        return price / size

    def is_interesting(self, expose):
        """True if price per square is below max price per square"""
        return self._pps(expose) <= self.max_pps


class PredicateFilter:
    """Include only those exposes satisfying the predicate"""

    def __init__(self, predicate):
        self.predicate = predicate

    def rejection_reason(self, expose):
        return "unknown"

    def is_interesting(self, expose):
        """True if predicate is satisfied"""
        return self.predicate(expose)


def get_distance_filter(config):
    max_durations = defaultdict(dict)
    for duration in config.get('durations', []):
        if 'destination' in duration and 'name' in duration:
            name = duration.get('name')
            for mode in duration.get('modes', []):
                if 'max_duration' in mode:
                    max_duration_minutes = mode['max_duration']
                    max_durations[name][mode['title']] = max_duration_minutes

    if len(max_durations) > 0:
        return Filter([MaxTravelDurationFilter(max_durations)])
    return Filter([])


class FilterBuilder:
    """Construct a filter chain"""

    def __init__(self):
        self.filters = []

    def read_config(self, config):
        """Adds filters from a config dictionary"""
        if "excluded_titles" in config:
            self.filters.append(TitleFilter(config["excluded_titles"]))
        if "filters" in config and config["filters"] is not None:
            filters_config = config["filters"]
            if "excluded_titles" in filters_config:
                self.filters.append(TitleFilter(filters_config["excluded_titles"]))
            if "min_price" in filters_config:
                self.filters.append(MinPriceFilter(filters_config["min_price"]))
            if "max_price" in filters_config:
                self.filters.append(MaxPriceFilter(filters_config["max_price"]))
            if "min_size" in filters_config:
                self.filters.append(MinSizeFilter(filters_config["min_size"]))
            if "max_size" in filters_config:
                self.filters.append(MaxSizeFilter(filters_config["max_size"]))
            if "min_rooms" in filters_config:
                self.filters.append(MinRoomsFilter(filters_config["min_rooms"]))
            if "max_rooms" in filters_config:
                self.filters.append(MaxRoomsFilter(filters_config["max_rooms"]))
            if "require_balcony" in filters_config:
                self.filters.append(HasBalconyFilter(filters_config["require_balcony"]))
            if "max_price_per_square" in filters_config:
                self.filters.append(PPSFilter(filters_config["max_price_per_square"]))
            if "wbs_flats_interesting" in filters_config:
                self.filters.append(WbsFilter(filters_config["wbs_flats_interesting"]))
        return self

    def max_size_filter(self, size):
        """Adds a max size filter"""
        self.filters.append(MaxSizeFilter(size))
        return self

    def predicate_filter(self, predicate):
        """Adds a predicate filter"""
        self.filters.append(PredicateFilter(predicate))
        return self

    def filter_already_seen(self, id_watch):
        """Filter exposes that have already been seen"""
        self.filters.append(AlreadySeenFilter(id_watch))
        return self

    def build(self):
        """Return the compiled filter"""
        return Filter(self.filters)


class Filter:
    """Abstract filter object"""

    __log__ = logging.getLogger('flathunt.filter')

    def __init__(self, filters):
        self.filters = filters

    def is_interesting_expose(self, expose):
        """Apply all filters to this expose"""
        for filter_ in self.filters:
            if not filter_.is_interesting(expose):
                return False

        return True

    def rejection_reasons(self, expose):
        """Apply all filters to this expose"""
        rejection_reasons = []
        for filter_ in self.filters:
            if not filter_.is_interesting(expose):
                try:
                    rejection_reasons.append(filter_.rejection_reason(expose))
                except Exception as e:
                    self.__log__.warning("Failed to print filter name", exc_info=e)
                    rejection_reasons.append("Reasoning Failure")

        return rejection_reasons

    def filter(self, exposes, id_watch):
        """Apply all filters to every expose in the list"""
        result = []
        for expose in exposes:
            interesting = self.is_interesting_expose(expose)
            if interesting:
                result.append(expose)
            else:
                rejection_reasons = self.rejection_reasons(expose)
                id_watch.set_expose_rejection_reason(expose, rejection_reasons)

        return result

    @staticmethod
    def builder():
        """Return a new filter builder"""
        return FilterBuilder()
