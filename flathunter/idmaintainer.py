"""SQLite implementation of IDMaintainer interface"""
import threading
import sqlite3 as lite
import datetime
import json
import logging
from typing import Optional, List

from flathunter.abstract_processor import Processor

__author__ = "Nody"
__version__ = "0.1"
__maintainer__ = "Nody"
__email__ = "harrymcfly@protonmail.com"
__status__ = "Prodction"

class SaveAllExposesProcessor(Processor):
    """Processor that saves all exposes to the database"""

    def __init__(self, config, id_watch):
        self.config = config
        self.id_watch = id_watch

    def process_expose(self, expose):
        """Save a single expose"""
        self.id_watch.save_expose(expose)
        return expose

class MarkSentExposes(Processor):
    """Processor that updates all given exposes with `was_sent = 1` """

    def __init__(self, config, id_watch):
        self.config = config
        self.id_watch = id_watch

    def process_expose(self, expose):
        """Save a single expose"""
        self.id_watch.mark_expose_as_sent(expose)
        return expose

class AlreadySeenFilter:
    """Filter exposes that have already been processed"""

    def __init__(self, id_watch):
        self.id_watch = id_watch

    def rejection_reason(self, expose):
        return "Already saw it"

    def is_interesting(self, expose):
        """Returns true if an expose should be kept in the pipeline"""
        if not self.id_watch.is_processed(int(expose['id'])):
            self.id_watch.mark_processed(expose['id'])
            return True
        return False

class IdMaintainer:
    """SQLite back-end for the database"""
    __log__ = logging.getLogger('flathunt')

    def __init__(self, db_name):
        self.db_name = db_name
        self.threadlocal = threading.local()

    def get_connection(self):
        """Connects to the SQLite database. Connections are thread-local"""
        connection = getattr(self.threadlocal, 'connection', None)
        if connection is None:
            try:
                self.threadlocal.connection = lite.connect(self.db_name)
                connection = self.threadlocal.connection
                cur = self.threadlocal.connection.cursor()
                cur.execute('CREATE TABLE IF NOT EXISTS processed (ID INTEGER)')
                cur.execute('CREATE TABLE IF NOT EXISTS executions (timestamp timestamp)')
                cur.execute('CREATE TABLE IF NOT EXISTS exposes (id INTEGER, created TIMESTAMP, \
                                    crawler STRING, details BLOB, was_sent INTEGER default 0, PRIMARY KEY (id, crawler))')
                try:
                    cur.execute('ALTER TABLE exposes ADD COLUMN was_sent INTEGER default 0')
                except lite.Error as e:
                    self.__log__.debug("Couldn't add was_sent column, probably new table %s:", e.args[0])
                try:
                    cur.execute('ALTER TABLE exposes ADD COLUMN rejection_reason STRING default 0')
                except lite.Error as e:
                    self.__log__.debug("Couldn't add rejection_reason column, probably new table %s:", e.args[0])
                cur.execute('CREATE TABLE IF NOT EXISTS users \
                                    (id INTEGER PRIMARY KEY, settings BLOB)')
                self.threadlocal.connection.commit()
            except lite.Error as error:
                self.__log__.error("Error %s:", error.args[0])
                raise error
        return connection

    def is_processed(self, expose_id):
        """Returns true if an expose has already been processed"""
        self.__log__.debug('is_processed(%d)', expose_id)
        cur = self.get_connection().cursor()
        cur.execute('SELECT id FROM processed WHERE id = ?', (expose_id,))
        row = cur.fetchone()
        return row is not None

    def mark_processed(self, expose_id):
        """Mark an expose as processed in the database"""
        self.__log__.debug('mark_processed(%d)', expose_id)
        cur = self.get_connection().cursor()
        cur.execute('INSERT INTO processed VALUES(?)', (expose_id,))
        self.get_connection().commit()

    def save_expose(self, expose):
        """Saves an expose to a database"""
        if not self._is_expose_already_saved(expose['id'], expose['crawler']):
            cur = self.get_connection().cursor()
            cur.execute('INSERT OR REPLACE INTO exposes(id, created, crawler, details) \
                         VALUES (?, ?, ?, ?)',
                        (int(expose['id']), datetime.datetime.now(),
                         expose['crawler'], json.dumps(expose)))
            self.get_connection().commit()

    def _is_expose_already_saved(self, id, crawler) -> bool:
        cur = self.get_connection().cursor()
        count = cur.execute('SELECT count(*) FROM exposes WHERE crawler = ? AND id = ? LIMIT 1', (crawler, int(id))).fetchone()[0]
        return count > 0

    def get_exposes_since(self, min_datetime: datetime.datetime, was_sent: Optional[str] = None):
        """Loads all exposes since the specified date"""
        def row_to_expose(row):
            obj = json.loads(row[2])
            obj['created_at'] = row[0]
            obj["rejection_reason"] = row[3]
            obj["was_sent"] = row[4] == "1"
            return obj
        cur = self.get_connection().cursor()
        was_sent_clause = ""
        if was_sent is not None:
            was_sent_clause = f"AND was_sent = {was_sent}"
        cur.execute(f'SELECT created, crawler, details, rejection_reason, was_sent FROM exposes \
                     WHERE created >= ? {was_sent_clause} ORDER BY created DESC', (min_datetime,))
        return list(map(row_to_expose, cur.fetchall()))

    def mark_expose_as_sent(self, expose):
        """Marks an expose by the specified crawler as sent-to-user"""
        cur = self.get_connection().cursor()
        cur.execute('UPDATE exposes SET was_sent = 1 WHERE crawler = ? AND id = ?',
                    (expose['crawler'], int(expose['id'])))
        if cur.rowcount != 1:
            self.__log__.error("Didn't find the expose to mark_expose_as_sent!")
        else:
            self.__log__.info(f"Set {expose['id']} as sent")
        self.get_connection().commit()

    def set_expose_rejection_reason(self, expose, rejection_reasons: List[str]):
        _id = int(expose['id'])
        old_rejection_reasons = self._get_expose_rejection_reasons(expose['id'])
        if old_rejection_reasons is None:
            old_rejection_reasons = []

        rejection_reasons = set([s.strip() for s in rejection_reasons])
        old_rejection_reasons = set(old_rejection_reasons)

        needs_update = rejection_reasons not in old_rejection_reasons

        if needs_update:
            updated_rejection_reasons = ", ".join(list(old_rejection_reasons | rejection_reasons))
            cur = self.get_connection().cursor()
            cur.execute('UPDATE exposes SET rejection_reason = ? WHERE crawler = ? AND id = ?',
                        (updated_rejection_reasons, expose['crawler'], _id))
            if cur.rowcount != 1:
                self.__log__.error("Didn't find the expose to mark as rejected!")
            else:
                url = expose["url"]
                self.__log__.info(f"Set rejection reasons for {_id} {url}: {updated_rejection_reasons}")
            self.get_connection().commit()

    def _get_expose_rejection_reasons(self, id: str) -> Optional[List[str]]:
        """Returns up to 'count' recent exposes, filtered by the provided filter"""
        cur = self.get_connection().cursor()
        cur.execute('SELECT rejection_reason FROM exposes WHERE id = ?', (id, ))
        rejection_reason = cur.fetchone()
        if rejection_reason is None or str(rejection_reason[0]) == "0":
            return None
        else:
            return [s.strip() for s in rejection_reason[0].strip().split(", ")]

    def get_recent_exposes(self, count, filter_set=None):
        """Returns up to 'count' recent exposes, filtered by the provided filter"""
        cur = self.get_connection().cursor()
        cur.execute('SELECT details FROM exposes ORDER BY created DESC')
        res = []
        next_batch = []
        while len(res) < count:
            if len(next_batch) == 0:
                next_batch = cur.fetchmany()
                if len(next_batch) == 0:
                    break
            expose = json.loads(next_batch.pop()[0])
            if filter_set is None or filter_set.is_interesting_expose(expose):
                res.append(expose)
        return res

    def save_settings_for_user(self, user_id, settings):
        """Saves the user settings to the database"""
        cur = self.get_connection().cursor()
        cur.execute('INSERT OR REPLACE INTO users VALUES (?, ?)', (user_id, json.dumps(settings)))
        self.get_connection().commit()

    def get_settings_for_user(self, user_id):
        """Loads the settings for a user from the database"""
        cur = self.get_connection().cursor()
        cur.execute('SELECT settings FROM users WHERE id = ?', (user_id,))
        row = cur.fetchone()
        if row is None:
            return None
        return json.loads(row[0])

    def get_user_settings(self):
        """Loads all users' settings from the database"""
        cur = self.get_connection().cursor()
        cur.execute('SELECT id, settings FROM users')
        res = []
        for row in cur.fetchall():
            res.append((row[0], json.loads(row[1])))
        return res

    def get_last_run_time(self):
        """Returns the time of the last hunt"""
        cur = self.get_connection().cursor()
        cur.execute("SELECT * FROM executions ORDER BY timestamp DESC LIMIT 1")
        row = cur.fetchone()
        if row is None:
            return None
        return datetime.datetime.strptime(row[0], '%Y-%m-%d %H:%M:%S.%f')

    def update_last_run_time(self):
        """Saves the time of the most recent hunt to the database"""
        cur = self.get_connection().cursor()
        result = datetime.datetime.now()
        cur.execute('INSERT INTO executions VALUES(?);', (result,))
        self.get_connection().commit()
        return result
