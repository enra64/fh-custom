"""Default Flathunter implementation for the command line"""
import logging
import traceback
from itertools import chain
import requests

from flathunter.config import Config
from flathunter.filter import Filter
from flathunter.processor import ProcessorChain


class Hunter:
    """Hunter class - basic methods for crawling and processing / filtering exposes"""
    __log__ = logging.getLogger('flathunt')

    def __init__(self, config: Config, id_watch):
        self.config = config
        if not isinstance(self.config, Config):
            raise Exception("Invalid config for hunter - should be a 'Config' object")
        self.id_watch = id_watch

    def crawl_for_exposes(self, max_pages=None):
        """Trigger a new crawl of the configured URLs"""

        def try_crawl(searcher, url, max_pages):
            try:
                return searcher.crawl(url, max_pages)
            except requests.exceptions.RequestException:
                self.__log__.info("Error while scraping url %s:\n%s", url, traceback.format_exc())
                return []

        return chain(*[try_crawl(searcher, url, max_pages)
                       for searcher in self.config.searchers()
                       for url in self.config.get('urls', [])])

    def hunt_flats(self, max_pages=None):
        """Crawl, process and filter exposes"""
        filter_set_reject_seen = Filter.builder() \
            .read_config(self.config) \
            .filter_already_seen(self.id_watch) \
            .build()
        filter_set_dont_reject_seen = Filter.builder() \
            .read_config(self.config) \
            .build()

        processor_chain = ProcessorChain.builder(self.config) \
            .save_all_exposes(self.id_watch) \
            .apply_filter(filter_set_reject_seen, self.id_watch) \
            .crawl_expose_details() \
            .apply_filter(filter_set_dont_reject_seen, self.id_watch) \
            .resolve_addresses() \
            .calculate_durations(self.id_watch) \
            .send_messages() \
            .mark_sent_exposes(self.id_watch) \
            .build()

        result = []

        # We need to iterate over this list to force the evaluation of the pipeline
        for expose in processor_chain.process(self.crawl_for_exposes(max_pages)):
            self.__log__.info('New offer %s from %s: %s', expose['id'], expose['crawler'], expose['title'])
            result.append(expose)

        # self.__log__.info(f"Had {len(result)} new exposes this run")
        self.id_watch.update_last_run_time()

        return result
