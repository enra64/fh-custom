"""Expose crawler for ImmobilienScout"""
import logging
import re
from typing import Optional, List, Dict

import requests
from flathunter.abstract_crawler import Crawler


class CrawlMobileImmobilienscout(Crawler):
    """Implementation of Crawler interface for ImmobilienScout"""

    __log__ = logging.getLogger('flathunt')
    URL_PATTERN = re.compile(r'https://(www\.)?api\.mobile\.immobilienscout24\.de')
    RESULT_LIMIT = 50
    VERIFY = True

    def __init__(self, config):
        super().__init__(config)
        logging.getLogger("requests").setLevel(logging.WARNING)
        self.config = config
        self.driver = None
        self.checkbox = None
        self.afterlogin_string = None
        self.token = None
        self.client_secret = self.config["immoscout_mobile_client_secret"]

    def _get_token(self) -> Optional[str]:
        url = "https://publicauth.immobilienscout24.de/oauth/token"
        payload = f'client_secret={self.client_secret}&client_id=AndroidApp-QuickCheckKey&grant_type=client_credentials'
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        try:
            response = requests.request("POST", url, headers=headers, data=payload, verify=self.VERIFY)
            result = response.json()
            token = result["access_token"]
            self.token = token
            return token
        except Exception as e:
            self.__log__.error("Failed to get token for is24 mobile api", exc_info=e)
            return None

    def _query_single_object(self, expose_id: str):
        if self.token is None:
            return None

        headers = {
            'User-Agent': 'ImmoScout24_1277_29_._',
            'Authorization': f'Bearer {self.token}'
        }
        search_url = "https://api.mobile.immobilienscout24.de/expose/" + str(expose_id)
        try:
            response = requests.request("GET", search_url, headers=headers, data={}, verify=self.VERIFY)
            result = response.json()
            return result
        except Exception as e:
            self.__log__.error("Failed to query is24 mobile api for single object", exc_info=e)
            return None

    def _query_api(self, search_url: str, token: str):
        headers = {
            'User-Agent': 'ImmoScout24_923_28_._',
            'Authorization': f'Bearer {token}'
        }

        try:
            response = requests.request("GET", search_url, headers=headers, data={}, verify=self.VERIFY)
            result = response.json()
            return result["results"]
        except Exception as e:
            self.__log__.error("Failed to query is24 mobile api", exc_info=e)
            return []

    def _find_price(self, attributes: List[Dict[str, str]]) -> str:
        for attribute in attributes:
            val = attribute["value"]
            if '€' in val:
                return val
        return "couldn't find price"

    def _find_rooms(self, attributes: List[Dict[str, str]]) -> str:
        for attribute in attributes:
            val = attribute["value"]
            if 'Zi' in val:
                return val
        return "couldn't find rooms"

    def _find_sqm(self, attributes: List[Dict[str, str]]) -> str:
        for attribute in attributes:
            val = attribute["value"]
            if 'm²' in val:
                return val
        return "couldn't find sqm"

    def _decode_requires_wbs_param(self, search_url) -> Optional[bool]:
        if "haspromotion=true" in search_url:
            return True
        elif "haspromotion=false" in search_url:
            return False
        else:
            return None

    def get_results(self, search_url, max_pages=None):
        """Loads the exposes from the ImmoScout site, starting at the provided URL"""

        token = self._get_token()
        if token is None:
            return []

        entries = self._query_api(search_url, token)

        result = []
        for flat in entries:
            image = None
            if "titlePicture" in flat and "full" in flat["titlePicture"]:
                image = flat["titlePicture"]["full"]
            title = ""
            if "title" in flat:
                title = flat["title"]
            address = ""
            if "address" in flat and "line" in flat["address"]:
                address = flat["address"]["line"]

            result.append(
                {
                    'id': int(flat["id"]),
                    'image': image,
                    'url': "https://www.api.mobile.immobilienscout24.de/expose/" + str(flat["id"]),
                    'title': title,
                    'price': self._find_price(flat["attributes"]),
                    'size': self._find_sqm(flat["attributes"]),
                    'rooms': self._find_rooms(flat["attributes"]),
                    'address': address,
                    'crawler': "is24_mobile",
                    'from': None,
                    'has_balcony': None,
                    'requires_wbs': self._decode_requires_wbs_param(search_url)
                }
            )

        return result

    def get_expose_details(self, expose):
        details = self._query_single_object(expose["id"])
        expose["url"] = "https://www.immobilienscout24.de/expose/" + str(expose["id"])

        if details is None:
            return expose

        try:
            sections = details["sections"]

            try:
                attribute_list = list(filter(lambda s: s["type"] == "ATTRIBUTE_LIST", sections))[0]["attributes"]
                expose["has_balcony"] = "alkon" in str(details) or "erasse" in str(details)

                free_from = list(filter(lambda a: a["label"] == "Bezugsfrei ab:", attribute_list))
                if len(free_from) > 0:
                    expose["from"] = free_from[0]["text"]
                else:
                    expose["from"] = "N/A"

            except Exception as e:
                self.__log__.error("Couldn't get attributes section", exc_info=e)

            try:
                media_section = list(filter(lambda s: s["type"] == "MEDIA", sections))[0]
                media_list = media_section["media"]
                expose["photos"] = [m["fullImageUrl"].split("/ORIG")[0] for m in media_list if m["type"] == "PICTURE"]
            except Exception as e:
                self.__log__.error("Couldn't get media section", exc_info=e)

        except Exception as e:
            self.__log__.error("Couldn't get detail sections", exc_info=e)

        return expose

    def extract_data(self, soup):
        pass
