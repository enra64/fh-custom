"""Functions and classes related to sending Telegram messages"""
import json
import logging
import requests

from flathunter.abstract_processor import Processor


class SenderTelegram(Processor):
    """Expose processor that sends Telegram messages"""
    __log__ = logging.getLogger('flathunt')

    def __init__(self, config, receivers=None):
        self.config = config
        self.bot_token = self.config.get('telegram', {}).get('bot_token', '')
        if receivers is None:
            self.receiver_ids = self.config.get('telegram', {}).get('receiver_ids', [])
        else:
            self.receiver_ids = receivers
            self.__log__.info(f"Receiver ID override: {self.receiver_ids}")

    def process_expose(self, expose):
        """Send a message to a user describing the expose"""
        message = self.config.get('message', "").format(
            title=expose['title'],
            rooms=expose['rooms'],
            size=expose['size'],
            price=expose['price'],
            url=expose['url'],
            address=expose['address'],
            free_from="N/A" if 'from' not in expose else expose['from'],
            has_balcony="N/A" if 'has_balcony' not in expose else str(expose['has_balcony']),
            requires_wbs="N/A" if 'requires_wbs' not in expose else str(expose['requires_wbs']),
            realtor="N/A" if 'realtor' not in expose else str(expose['realtor']),
            durations="" if 'durations' not in expose else expose['durations']
        ).strip()
        self.send_msg(message, expose)
        return expose

    def _clean(self, msg: str, ampersand_replacement="und") -> str:
        return msg \
            .replace("&", ampersand_replacement) \
            .replace("<", "&lt;") \
            .replace(">", "&gt;")

    def _send_markdown(self, chat_id, text: str = "", inline_markup: str = "",
                       silent: bool = False) -> requests.Response:
        if silent:
            disable_notification = "&disable_notification=true"
        else:
            disable_notification = "&disable_notification=false"

        url = 'https://api.telegram.org/bot%s/sendMessage?chat_id=%i&parse_mode=HTML&text=%s%s' + disable_notification
        qry = url % (self.bot_token, chat_id, text, inline_markup)
        return requests.get(qry)

    def _send_error(self, chat_id, text: str) -> requests.Response:
        url = 'https://api.telegram.org/bot%s/sendMessage?chat_id=%i&text=%s'
        qry = url % (self.bot_token, chat_id, text)
        return requests.get(qry)

    def send_local_photo(self, file, silent=True):
        if self.receiver_ids is None:
            return
        if silent:
            disable_notification = "&disable_notification=true"
        else:
            disable_notification = "&disable_notification=false"

        for chat_id in self.receiver_ids:
            result = requests.post(
                url=f'https://api.telegram.org/bot{self.bot_token}/sendPhoto?chat_id={chat_id}{disable_notification}',
                files={'photo': file}
            )
            data = result.json()
            self.__log__.debug(f"sent photo {data}")
            file.seek(0)

    def send_msg(self, message, expose=None, silent=False, local_photos=None):
        """Send messages to each of the receivers in receiver_ids"""
        if self.receiver_ids is None:
            return

        for chat_id in self.receiver_ids:
            text = self._clean(message)
            if expose is not None and expose['crawler'] is not None:
                text = f"<b>{expose['crawler']}</b>: " + text

            inline_markup = ""
            if expose is not None:
                keyboard = {
                    "inline_keyboard": [
                        [
                            {"text": "Angebot", "url": expose['url']},
                            {"text": "Google Maps", "url": "https://maps.google.com"},
                        ]
                    ]
                }

                inline_markup = f"&reply_markup={json.dumps(keyboard)}"

            self.__log__.debug(('token:', self.bot_token))
            self.__log__.debug(('chatid:', chat_id))
            self.__log__.debug(('text', text))

            resp = self._send_markdown(chat_id, text, inline_markup, silent=silent)

            if expose is not None and 'address' in expose and not silent:
                self._send_markdown(chat_id, expose['address'])

            self.__log__.debug("Got response (%i): %s", resp.status_code, resp.content)
            data = resp.json()

            # handle error
            if not resp.ok:
                status_code = resp.status_code
                desc = data.get("description", "n/a")

                if "message is too long" in desc:
                    chunk = ""
                    for line in text.split("\n"):
                        chunklen = len(chunk)
                        linelen = len(line)
                        if chunklen + linelen > 3500:
                            self._send_markdown(chat_id, chunk, silent=silent)
                            chunk = ""
                        else:
                            chunk += line + "\n"
                else:
                    error = "Unexpected telegram error, status %i, message: %s" % (status_code, desc)
                    self.__log__.error(error)
                    self._send_error(chat_id, error)
            else:
                if expose is None or 'photos' not in expose or len(expose['photos']) == 0:
                    continue

                original_id = data['result']['message_id']
                url_list = []
                for photo in expose['photos']:
                    url_list.append({
                        "type": "photo",
                        "media": photo
                    })

                def chunks(lst, n):
                    """Yield successive n-sized chunks from lst."""
                    for i in range(0, len(lst), n):
                        yield lst[i:i + n]

                for chunk in chunks(url_list, 10):
                    media_urls = json.dumps(chunk)
                    resp = requests.get(
                        f"https://api.telegram.org/"
                        f"bot{self.bot_token}/sendMediaGroup?"
                        f"chat_id={chat_id}&"
                        f"disable_notification=true&"
                        f"reply_to_message_id={original_id}&"
                        f"media={media_urls}"
                    )

                    if resp.status_code != 200:
                        error = "When sending bot image, we got error: %s" % (resp.content)
                        self.__log__.error(error)
