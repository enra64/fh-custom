import datetime
import logging
import tempfile
import threading
import sqlite3 as lite
from collections import Counter


from flathunter.config import Config
from flathunter.filter import ExposeHelper
from flathunter.idmaintainer import IdMaintainer
from flathunter.sender_telegram import SenderTelegram


def _interval_to_max_age(interval):
    """Transform the string interval to max heartbeat age in seconds"""
    if interval is None:
        return None
    if interval.lower() == 'five_seconds':
        return 5
    if interval.lower() == 'hour':
        return 60 * 60
    if interval.lower() == 'day':
        return 60 * 60 * 24
    if interval.lower() == 'week':
        return 60 * 60 * 24 * 7
    raise Exception(f"{interval} is not a valid heartbeat interval - no heartbeat messages will be sent.")


class AdvancedHeartbeat:
    """heartbeat class - Will inform the user on regular intervals whether the bot is still alive"""
    __log__ = logging.getLogger('flathunt')

    def __init__(self, config, interval):
        self.config = config
        if not isinstance(self.config, Config):
            raise Exception("Invalid config for hunter - should be a 'Config' object")
        self.notifiers = self.config.get('notifiers', [])
        if 'telegram' in self.notifiers:
            self.notifier = SenderTelegram(config)
        else:
            raise Exception("No telegram configured, this doesn't make any sense")
        self.max_heartbeat_age_seconds = _interval_to_max_age(interval)
        self.interval = interval

        self.db_name = f'{config.database_location()}/heartbeat.db'
        self.threadlocal = threading.local()

    def _get_last_heartbeat_time(self) -> datetime.datetime:
        """Returns the timestamp when the last heartbeat was sent"""
        cur = self.get_connection().cursor()
        cur.execute('SELECT heartbeat_time FROM last_heartbeat_time ORDER BY heartbeat_time DESC LIMIT 1')
        row = cur.fetchone()
        if row is None:
            return datetime.datetime.min
        else:
            return row[0]

    def _update_last_heartbeat_time(self):
        cur = self.get_connection().cursor()
        cur.execute('INSERT OR REPLACE INTO last_heartbeat_time(heartbeat_time) \
                     VALUES (?)', (datetime.datetime.now(),))
        self.get_connection().commit()

    def _get_heartbeat_age_seconds(self) -> float:
        return (datetime.datetime.now() - self._get_last_heartbeat_time()).total_seconds()

    def _is_heartbeat_too_old(self) -> bool:
        return self._get_heartbeat_age_seconds() > self.max_heartbeat_age_seconds

    def get_connection(self):
        """Connects to the SQLite database. Connections are thread-local"""
        connection = getattr(self.threadlocal, 'connection', None)
        if connection is None:
            try:
                self.__log__.info(f"advanced heartbeat using db @ {self.db_name}")
                self.threadlocal.connection = lite.connect(self.db_name, detect_types=lite.PARSE_DECLTYPES)
                connection = self.threadlocal.connection
                cur = self.threadlocal.connection.cursor()
                cur.execute('CREATE TABLE IF NOT EXISTS last_heartbeat_time (heartbeat_time TIMESTAMP)')
                self.threadlocal.connection.commit()
            except lite.Error as error:
                self.__log__.error("Error %s:", error.args[0])
                raise error
        return connection

    def send_alive_message(self):
        self.notifier.send_msg(
            message="INFO: Bot started. If this happens a lot, contact a maintainer.",
            silent=True
        )

    def send_heartbeat_if_required(self, id_maintainer: IdMaintainer):
        """Send a new heartbeat message if required"""
        if self.notifier is not None and self.interval is not None and self._is_heartbeat_too_old():
            last_heartbeat = self._get_last_heartbeat_time()
            new_exposes_overall = id_maintainer.get_exposes_since(last_heartbeat, was_sent=None)
            new_exposes_overall_count = len(new_exposes_overall)
            new_exposes_sent = id_maintainer.get_exposes_since(last_heartbeat, was_sent="1")
            new_exposes_sent_count = len(new_exposes_sent)
            new_exposes_unsent = id_maintainer.get_exposes_since(last_heartbeat, was_sent="0")
            new_exposes_unsent_count = len(new_exposes_unsent)

            is24_mobile_sent_count = len([e for e in new_exposes_sent if e['crawler'] == "is24_mobile"])
            kleinanzeigen_sent_count = len([e for e in new_exposes_sent if e['crawler'] == "Kleinanzeigen"])
            immowelt_sent_count = len([e for e in new_exposes_sent if e['crawler'] == "ImmoWelt"])
            wggesucht_sent_count = len([e for e in new_exposes_sent if e['crawler'] == "Wg-Gesucht"])
            coreberlin_sent_count = len([e for e in new_exposes_sent if e['crawler'] == "coreberlin"])
            inberlinwohnen_sent_count = len([e for e in new_exposes_sent if e['crawler'] == "inberlinwohnen"])

            is24_mobile_overall_count = len([e for e in new_exposes_overall if e['crawler'] == "is24_mobile"])
            kleinanzeigen_overall_count = len([e for e in new_exposes_overall if e['crawler'] == "Kleinanzeigen"])
            immowelt_overall_count = len([e for e in new_exposes_overall if e['crawler'] == "ImmoWelt"])
            wggesucht_overall_count = len([e for e in new_exposes_overall if e['crawler'] == "Wg-Gesucht"])
            coreberlin_overall_count = len([e for e in new_exposes_overall if e['crawler'] == "coreberlin"])
            inberlinwohnen_overall_count = len([e for e in new_exposes_overall if e['crawler'] == "inberlinwohnen"])

            rejection_reasons = []
            rejected_price_distribution = []
            accepted_price_distribution = []
            if new_exposes_overall_count == 0:
                msg = ("Heartbeat. Your bot is still running. "
                       "Since the last heartbeat, no new flats were found on your platforms.")
            else:
                if new_exposes_overall_count == 1:
                    msg = 'Heartbeat. Your bot is still running. ' + \
                          f'Since the last heartbeat a single new flat was found, '
                else:
                    msg = 'Heartbeat. Your bot is still running. ' + \
                          f'Since the last heartbeat, {new_exposes_overall_count} new flats were found, '

                if new_exposes_sent_count > 0:
                    stats = []
                    if is24_mobile_overall_count > 0:
                        stats.append(f'IS24: {is24_mobile_sent_count}/{is24_mobile_overall_count}')
                    if kleinanzeigen_overall_count > 0:
                        stats.append(f'KA: {kleinanzeigen_sent_count}/{kleinanzeigen_overall_count}')
                    if wggesucht_overall_count > 0:
                        stats.append(f'WgGe: {wggesucht_sent_count}/{wggesucht_overall_count}')
                    if coreberlin_overall_count > 0:
                        stats.append(f'CoreBerlin: {coreberlin_sent_count}/{coreberlin_overall_count}')
                    if immowelt_overall_count > 0:
                        stats.append(f'Immowelt: {immowelt_sent_count}/{immowelt_overall_count}')
                    if inberlinwohnen_sent_count > 0:
                        stats.append(f'Inberlinwohnen: {inberlinwohnen_sent_count}/{inberlinwohnen_overall_count}')
                    stats_formatted = ", ".join(stats)
                    msg += f'of which {new_exposes_sent_count} passed through your filter set: {stats_formatted}'

                    for expose in new_exposes_sent:
                        size = ExposeHelper.get_size(expose)
                        price = ExposeHelper.get_price(expose)
                        accepted_price_distribution.append((size, price))
                else:
                    msg += "none of which passed through your filter set. "

                if new_exposes_unsent_count > 0:
                    for expose in new_exposes_unsent:
                        if len(expose['rejection_reason']) > 0 and not expose["was_sent"]:
                            reason = expose['rejection_reason']
                            split_reasons = reason.split(", ")

                            for r in split_reasons:
                                if r != "Already saw it":
                                    rejection_reasons.append(r)

                            size = ExposeHelper.get_size(expose)
                            price = ExposeHelper.get_price(expose)

                            if size is not None and price is not None:
                                rejected_price_distribution.append((size, price))
                        else:
                            self.__log__.warning(f"{expose['id']} was not sent, but had an empty rejection reason.")

                    forbidden_titles = list(filter(lambda t: "forbidden title" in t, rejection_reasons))
                    if len(forbidden_titles) > 0:
                        forbidden_titles = [f.replace("forbidden title ", "").replace("(", "").replace(")", "") for f in
                                            forbidden_titles]
                        forbidden_titles = [t.title().replace("Wbs", "WBS").replace("wbs", "WBS") for t in forbidden_titles]
                        forbidden_titles = list(set(forbidden_titles))
                        titles_formatted = ", ".join(forbidden_titles)
                        msg += f"\n\nThe following {len(forbidden_titles)} forbidden title words were found & rejected: {titles_formatted}."

            self.notifier.send_msg(msg, silent=True)
            self.__log__.info(msg)
            self._update_last_heartbeat_time()

            try:
                import pygal
                import cairosvg
                xy_chart = pygal.XY(
                    title="Size/Price Distribution of Flats",
                    x_title="Size",
                    y_title="Price",
                    legend_at_bottom=True,
                )
                if "filters" in self.config and self.config["filters"] is not None:
                    filters_config = self.config["filters"]
                    if "min_price" in filters_config:
                        xy_chart.add(f"minprice ({filters_config['min_price']}€)", [(filters_config["min_size"], filters_config["min_price"]),
                                                  (filters_config["max_size"], filters_config["min_price"])],
                                     show_dots=False)
                    if "max_price" in filters_config:
                        xy_chart.add(f"maxprice ({filters_config['max_price']}€)", [(filters_config["min_size"], filters_config["max_price"]),
                                                  (filters_config["max_size"], filters_config["max_price"])],
                                     show_dots=False)
                    if "min_size" in filters_config:
                        xy_chart.add(f"minsize ({filters_config['min_size']})", [(filters_config["min_size"], filters_config["min_price"]),
                                                 (filters_config["min_size"], filters_config["max_price"])],
                                     show_dots=False)
                    if "max_size" in filters_config:
                        xy_chart.add(f"maxsize ({filters_config['max_size']})", [(filters_config["max_size"], filters_config["min_price"]),
                                                 (filters_config["max_size"], filters_config["max_price"])],
                                     show_dots=False)

                xy_chart.add("Rejected", rejected_price_distribution, stroke=False)
                xy_chart.add("Accepted", accepted_price_distribution, stroke=False)

                with tempfile.TemporaryFile(mode="w+b") as f:
                    try:
                        cairosvg.svg2png(
                            bytestring=xy_chart.render(),
                            write_to=f,
                            dpi=300
                        )
                        f.seek(0)
                        try:
                            self.notifier.send_local_photo(f, silent=True)
                        except Exception as e:
                            self.__log__.warning(
                                "Unexpected error during rejected_price_distribution graph sending", exc_info=e)
                    except Exception as e:
                        self.__log__.warning("Unexpected error during rejected_price_distribution graph rendering",
                                             exc_info=e)

            except Exception as e:
                self.__log__.warning("Unexpected error w/ rejected_price_distribution graph temp file creation",
                                     exc_info=e)

            if len(rejection_reasons) > 0:
                try:
                    import pygal
                    import cairosvg
                    def group_rejection_reasons(reason):
                        return reason.split("(")[0].strip()

                    rejection_reasons = map(group_rejection_reasons, rejection_reasons)
                    countmap = Counter(rejection_reasons)
                    xy_chart = pygal.HorizontalBar()
                    xy_chart.title = 'Rejection Reason Count'
                    for reason, count in countmap.most_common():
                        xy_chart.add(reason, count)

                    with tempfile.TemporaryFile(mode="w+b") as f:
                        try:
                            cairosvg.svg2png(
                                bytestring=xy_chart.render(),
                                write_to=f,
                                dpi=300
                            )
                            f.seek(0)
                            try:
                                self.notifier.send_local_photo(f, silent=True)
                            except Exception as e:
                                self.__log__.warning("Unexpected error during graph sending", exc_info=e)
                        except Exception as e:
                            self.__log__.warning("Unexpected error during graph rendering", exc_info=e)

                except Exception as e:
                    self.__log__.warning("Unexpected error w/ graph temp file creation", exc_info=e)
