"""Calculate Google-Maps distances between specific locations and the target flat"""
import logging
import datetime
import time
import urllib
from collections import defaultdict
from typing import Any, Tuple

import requests

from flathunter.abstract_processor import Processor

class GMapsDurationProcessor(Processor):
    """Implementation of Processor class to calculate travel durations"""

    GM_MODE_TRANSIT = 'transit'
    GM_MODE_BICYCLE = 'bicycling'
    GM_MODE_DRIVING = 'driving'

    __log__ = logging.getLogger('flathunt')

    def __init__(self, config):
        self.config = config

    def process_expose(self, expose):
        """Calculate the durations for an expose"""
        formatted_addresses, distance_durations = self.get_formatted_durations(expose['address'])
        expose['durations'] = formatted_addresses.strip()
        expose['machine_readable_durations'] = distance_durations
        return expose

    def get_formatted_durations(self, address):
        """Return a formatted list of GoogleMaps durations"""
        out = ""
        out_durations = defaultdict(dict)
        for duration in self.config.get('durations', []):
            if 'destination' in duration and 'name' in duration:
                dest = duration.get('destination')
                name = duration.get('name')
                for mode in duration.get('modes', []):
                    if 'gm_id' in mode and 'title' in mode \
                                       and 'key' in self.config.get('google_maps_api', {}):
                        duration, seconds = self.get_gmaps_distance(address, dest, mode['gm_id'])

                        if duration is None or seconds is None:
                            continue

                        title = mode['title']
                        out_durations[name][title] = seconds / 60

                        out += f"{title} {name}: {duration}\n"
                        time.sleep(0.500)

        return out.strip(), out_durations

    def get_gmaps_distance(self, address, dest, mode) -> Tuple[Any, Any]:
        """Get the distance"""
        # get timestamp for next monday at 9:00:00 o'clock
        now = datetime.datetime.today().replace(hour=9, minute=0, second=0)
        next_monday = now + datetime.timedelta(days=(7 - now.weekday()))
        arrival_time = str(int(time.mktime(next_monday.timetuple())))

        # decode from unicode and url encode addresses
        address = urllib.parse.quote_plus(address.strip().encode('utf8'))
        dest = urllib.parse.quote_plus(dest.strip().encode('utf8'))
        self.__log__.debug("Got address: %s", address)

        # get google maps config stuff
        base_url = self.config.get('google_maps_api', {}).get('url')
        gm_key = self.config.get('google_maps_api', {}).get('key')

        if not gm_key and mode != self.GM_MODE_DRIVING:
            self.__log__.warning("No Google Maps API key configured and without using a mode "
                                 "different from 'driving' is not allowed. "
                                 "Downgrading to mode 'drinving' thus. ")
            mode = 'driving'
            base_url = base_url.replace('&key={key}', '')

        # retrieve the result
        url = base_url.format(dest=dest, mode=mode, origin=address,
                              key=gm_key, arrival=arrival_time)
        result = requests.get(url).json()
        if result['status'] == "OVER_QUERY_LIMIT":
            self.__log__.warning("Reached GMaps query limit")
            return None, None
        elif result['status'] != 'OK':
            self.__log__.error("Failed retrieving distance to address %s: %s", address, result)
            return None, None

        # get the fastest route
        distances = {}
        for row in result['rows']:
            for element in row['elements']:
                if 'status' in element and element['status'] != 'OK':
                    self.__log__.warning("For address %s we got the status message: %s",
                                         address, element['status'])
                    self.__log__.debug("We got this result: %s", repr(result))
                    continue
                travel_duration_seconds = element['duration']['value']
                self.__log__.debug("Got distance and duration: %s / %s (%i seconds)",
                                   element['distance']['text'],
                                   element['duration']['text'],
                                   travel_duration_seconds)
                duration_text = element['duration']['text']
                distance_text = element['distance']['text']
                distances[travel_duration_seconds] = f"{duration_text} ({distance_text})"

        if len(distances) > 0:
            travel_duration_seconds = min(distances.keys())
            return distances[travel_duration_seconds], travel_duration_seconds
        else:
            return None, None
