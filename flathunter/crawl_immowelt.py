"""Expose crawler for ImmoWelt"""
import re
import datetime
import hashlib

from bs4 import BeautifulSoup, Tag

import logging
from flathunter.abstract_crawler import Crawler

class CrawlImmowelt(Crawler):
    """Implementation of Crawler interface for ImmoWelt"""

    __log__ = logging.getLogger('flathunt')
    URL_PATTERN = re.compile(r'https://www\.immowelt\.de')

    def __init__(self, config):
        super().__init__(config)
        self.config = config

    def get_expose_details(self, expose):
        """Loads additional details for an expose by processing the expose detail URL"""
        soup = self.get_page(expose['url'])
        expose['from'] = "N/A"

        details = soup.select("app-estate-object-informations p")
        for detail in details:
            if detail.text.strip() == "Bezug":
                expose['from'] = detail.findNext("p").text.strip()
                break

        try:
            candidate = " ".join([t.text.strip() for t in soup.select("#exposeAddress span")])
            candidate = candidate.replace("Straße nicht freigegeben", "")
            expose['address'] = candidate
        except Exception as e:
            self.__log__.error("Couldn't get expose address", exc_info=e)

        try:
            details = soup.select("#mainGallery picture img")

            photos = []
            for detail_tag in details:
                try:
                    tag_attrs = detail_tag.attrs
                    if "src" in tag_attrs:
                        src = tag_attrs["src"]
                        photos.append(src)
                except Exception as e:
                    self.__log__.error("Couldn't get expose photo", exc_info=e)

            if len(photos) > 0:
                expose["photos"] = photos
        except Exception as e:
            self.__log__.error("Couldn't get expose photos", exc_info=e)

        try:
            accessories = list(map(lambda t: t.text.strip(), soup.select("div .card-content ul li")))

            if "Balkon" in accessories or "Terrasse" in accessories:
                expose["has_balcony"] = True
            else:
                expose["has_balcony"] = False
        except Exception as e:
            self.__log__.error("Couldn't fetch balcony info for immowelt", exc_info=e)


        return expose

    # pylint: disable=too-many-locals
    def extract_data(self, soup: BeautifulSoup):
        """Extracts all exposes from a provided Soup object"""
        entries = []
        soup_res = soup.find("main")
        if not isinstance(soup_res, Tag):
            return []

        title_elements = soup_res.find_all("h2")
        expose_ids = soup_res.find_all("a", id=True)

        for idx, title_el in enumerate(title_elements):
            try:
                price = expose_ids[idx].find(
                    "div", attrs={"data-test": "price"}).text
            except IndexError:
                price = ""

            try:
                size = expose_ids[idx].find(
                    "div", attrs={"data-test": "area"}).text
            except IndexError:
                size = ""

            try:
                rooms = expose_ids[idx].find(
                    "div", attrs={"data-test": "rooms"}).text
            except IndexError:
                rooms = ""

            try:
                url = expose_ids[idx].get("href")
            except IndexError:
                continue

            picture = expose_ids[idx].find("picture")
            image = None
            if picture:
                src = picture.find("source")
                if src:
                    image = src.get("data-srcset")

            try:
                address = expose_ids[idx].find(
                    "div", attrs={"class": re.compile("IconFact.*")}
                  )
                address = address.find("span").text
            except (IndexError, AttributeError):
                address = ""

            processed_id = int(
              hashlib.sha256(expose_ids[idx].get("id").encode('utf-8')).hexdigest(), 16
            ) % 10**16

            details = {
                'id': processed_id,
                'image': image,
                'url': url,
                'title': title_el.text.strip(),
                'rooms': rooms,
                'price': price,
                'size': size,
                'address': address,
                'crawler': "ImmoWelt"
            }
            entries.append(details)

        self.__log__.debug('Number of entries found: %d', len(entries))

        return entries