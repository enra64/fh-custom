"""Expose crawler for inberlinwohnen"""
import logging
import re

from flathunter.abstract_crawler import Crawler
from flathunter.coreberlin.scraper.flats import get_flat_data_from_soup
from flathunter.coreberlin.scraper.model.flat import flat_to_fh_dict


class CrawlCoreBerlin(Crawler):
    """Implementation of Crawler interface for Ebay Kleinanzeigen"""

    __log__ = logging.getLogger('flathunt')
    USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'
    URL_PATTERN = re.compile(r'(core-berlin)')

    def __init__(self, config):
        super().__init__(config)
        logging.getLogger("requests").setLevel(logging.WARNING)
        self.config = config

    def get_page(self, search_url, driver=None, page_no=None):
        """Applies a page number to a formatted search URL and fetches the exposes at that page"""
        return self.get_soup_from_url(search_url)

    # pylint: disable=too-many-locals
    def extract_data(self, soup):
        """Extracts all exposes from a provided Soup object"""
        flats = get_flat_data_from_soup(soup)
        entries = list(map(lambda f: flat_to_fh_dict(f), flats))

        self.__log__.debug('extracted: %d', len(entries))

        return entries

    def get_expose_details(self, expose):
        """Loads additional details for an exposé. Should be implemented in the subclass"""
        source = expose['url']
        soup = self.get_page(source)
        expose['photos'] = []

        image_elements = soup.select('#jea-gallery img')

        for img in image_elements:
            photo = img.attrs["src"]
            expose['photos'].append("https://core-berlin.de" + photo)

        try:
            expose['price'] = \
                [rent.text.split()[1] for rent in soup.select('table')[0] if "Gesamtmiete" in rent.text][0]
        except:
            pass

        return expose
