import traceback
from typing import List

from bs4 import BeautifulSoup

from flathunter.coreberlin.scraper.model.flat import Flat


def get_room_count(child):
    try:
        text_elements = child.select(".row > div.nine")[0].findAll(text=True, recursive=False)
        for el in text_elements:
            if "Zimmer" in el:
                sqm = str(el).strip().split()[1]
                return sqm
        return "N/A (1)"
    except:
        traceback.print_exc()
        return "N/A (2)"


def get_balcony(child):
    try:
        attributes = child.select(".row > div.nine")[0].contents[5].text
        return "Balkon" in attributes
    except:
        traceback.print_exc()
        return "N/A"


def get_wbs_required(child):
    return None

def get_area_sqm(child):
    try:
        text_elements = child.select(".row > div.nine")[0].findAll(text=True, recursive=False)
        for el in text_elements:
            if "Zimmer" in el:
                sqm = str(el).strip().split()[3]
                return sqm
        return "N/A (1)"
    except:
        traceback.print_exc()
        return "N/A (2)"


def get_price(child):
    try:
        for rent_candidate in child.select("div.nine")[0].contents:
            if hasattr(rent_candidate, "text") and "Kaltmiete" in rent_candidate.text:
                return rent_candidate.text.split()[2]
        return "0"
    except:
        traceback.print_exc()
        return "unable to get price"

def get_title(child):
    try:
        return " ".join(child.select("div.nine h4")[0].text.strip().split()).replace("*", "")
    except:
        traceback.print_exc()
        return "N/A"


def get_address(child):
    try:
        return " ".join(child.select(".row > div.nine")[0].contents[3].contents[2].split())
    except:
        traceback.print_exc()
        return "unable to get address "


def get_link(child):
    try:
        return "https://www.core-berlin.de" + child.attrs["href"]
    except:
        traceback.print_exc()
        return "unable to get link"


def get_image(child):
    try:
        src = child.select("img")[0].attrs["src"]
        return "https://www.core-berlin.de" + src
    except:
        traceback.print_exc()
        return "unable to get image"


def get_available_from(child):
    return None


def get_id(child):
    try:
        return int(child.attrs['href'].replace("/de/vermietung/", "").split("-")[0])
    except:
        traceback.print_exc()
        return "unable to get id"


def soupify(html_doc):
    try:
        parsed = BeautifulSoup(html_doc.content, "html.parser")
        try:
            children_tags = parsed.select("#_tb_relevant_results > li")
            return children_tags
        except:
            traceback.print_exc()
            print("couldn't select relevant results")
    except:
        traceback.print_exc()
        print("Couldn't create beautifulsoup")
    return None


def get_flat_data_from_soup(soup) -> List[Flat]:
    scrape = soup.select("#jForm > div.row >div > div > a")

    if scrape is None:
        print("ooph why no scrape?")
        return []

    result = []

    for child in scrape:
        result.append(
            Flat(
                get_id(child),
                get_room_count(child),
                get_area_sqm(child),
                get_title(child),
                get_price(child),
                get_address(child),
                get_link(child),
                get_available_from(child),
                get_image(child),
                get_balcony(child),
                get_wbs_required(child),
            )
        )

    return result

