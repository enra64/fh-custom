from dataclasses import dataclass
from typing import Dict


@dataclass
class Flat:
    id: str
    room_count: str
    area_sqm: str
    title: str
    price: str
    address: str
    link: str
    available_from: str
    image: str
    has_balcony: bool
    requires_wbs: bool


def flat_to_fh_dict(flat: Flat) -> Dict:
    return {
        'id': flat.id,
        'image': flat.image,
        'url': flat.link,
        'title': flat.title,
        'price': flat.price,
        'size': flat.area_sqm,
        'rooms': flat.room_count,
        'address': flat.address,
        'crawler': "coreberlin",
        'from': flat.available_from,
        'has_balcony': flat.has_balcony,
        'requires_wbs': flat.requires_wbs
    }