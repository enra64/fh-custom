FROM python:3.12-slim

RUN apt-get update && apt-get -qq -y --no-install-recommends install libcairo2 libcairo2-dev python3-lxml && rm -rf /var/lib/apt/lists/*

RUN groupadd -g 999 python && useradd -r -u 999 -g python python
RUN mkdir /app && chown python:python /app
WORKDIR /app

COPY --chown=python:python requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY --chown=python:python . /app

VOLUME /config
VOLUME /databases

USER 999
CMD [ "python3", "-u", "flathunt.py", "-c", "/config/config.yaml", "--heartbeat", "day" ]